# Namensschema

![Logo von OpenTT – Schläger mit Katze](../images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

Da die ITTF verschiedene Namensschemata verwendet (siehe [Geschichte](../history)), habe ich ein einheitliches Namensschema für alle LARCS entwickelt.

Jede LARC besitzt eine ID, die die LARC eindeutig identifiziert: Jahreszahl und Nummer der LARC in diesem Jahr.

`Jahr-#`

Beispiele:

- LARC 2007-2
	- diese LARC ist die zweite Liste des Jahres 2007
- LARC 2021-3
	- diese LARC ist die dritte Liste des Jahres 2021
