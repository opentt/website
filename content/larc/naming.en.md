# Naming Scheme

![Logo of OpenTT – Racket with Cat](../images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

Since the ITTF uses different naming schemes (see [history](../history)), I have developed a uniform naming scheme for all LARCS.

Each LARC has an IDs that uniquely identifies the LARC: a combination of the year and number of the LARC in that year:

`year-#`

Examples:

- LARC 2007-2
	- this LARC is the second list of the year 2007
- LARC 47 = LARC 2021-3
	- this LARC is the third list of the year 2021
