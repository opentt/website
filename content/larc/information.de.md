# Informationen

![Logo von OpenTT – Schläger mit Katze](../images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

- Definition von API, Daten und Datenbank
	- [Handbuch](https://opentt.gitlab.io/larc-api)
	- [Code](https://gitlab.com/opentt/larc-api)
	- [Issue-Tracker](https://gitlab.com/opentt/larc-api/-/issues)
- Kleiner API-Client
	- [Handbuch](https://opentt.gitlab.io/larc)
	- [Code](https://gitlab.com/opentt/larc)
	- [Issue-Tracker](https://gitlab.com/opentt/larc/-/issues)
- [LARC-Downloads bei TT-Schiri](https://www.tt-schiri.de/einsatzinformationen/belaglisten/)
