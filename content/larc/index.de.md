# LARC

![Logo von OpenTT – Schläger mit Katze](../images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

!!! tip ""
	Inoffizielle Belagliste.

Ohne große Vorrede:

- [API-Dokumentation incl. OpenAPI-Spec](/larc/api/)
- [API-Beispiel-Client](/larc/client/)

Die LARC des ITTF war zunächst eine PDF-Liste, jetzt ist es eine [Online-Liste](https://equipments.ittf.com/#/equipments/racket_coverings) mit PDF-Export.
Das ist zwar schon eine Verbesserung, was fehlt:

- REST-API für eine LARC-App
- seit zwei Jahren kompaktes PDF

Das störte mich seit mehreren Jahren, die ITTF reagiert auf entsprechende E-Mails nicht.
Also habe ich dieses Jahr die Sache in die Hand genommen und mir eine eigene API und App vorgenommen.

Das ist eine Menge auf einmal, daher ist der Plan wie folgt:

1. API-Definition, die inkrementell verbessert wird
2. Online-API-Client als PoC und Test der API
3. Webseite
4. Android-App

Die ersten drei Punkte sind in Arbeit, der vierte kommt mit der Konsolidierung der API und nachdem ich mir entsprechendes Know-How aufgebaut habe.
