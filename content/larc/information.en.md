# Information

![Logo of OpenTT – Racket with Cat](../images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

- definition of API, Data, and database
	- [manual](https://opentt.gitlab.io/larc-api)
	- [code](https://gitlab.com/opentt/larc-api)
	- [issue tracker](https://gitlab.com/opentt/larc-api/-/issues)
- small API client
	- [manual](https://opentt.gitlab.io/larc)
	- [code](https://gitlab.com/opentt/larc)
	- [issue tracker](https://gitlab.com/opentt/larc/-/issues)
- [LARC downloads at TT-Schiri](https://www.tt-schiri.de/einsatzinformationen/belaglisten/)
