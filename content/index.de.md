# Einleitung

![Logo von OpenTT – Schläger mit Katze](images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

!!! tip ""
	Offene Dokumente und Anwendungen für Tischtennis.

Diese Webseite bietet Anwendungen, Dokumente und Informationen rund um Tischtennis.
Der Fokus liegt dabei zunächst auf der Schiedsrichterei, das wird bei Bedarf auch gern erweitert.

- [Racket Control](/racket-control/) – Schlägertests für alle in einfach.

Alle Daten, Inhalte und Dokumente liegen im gitlab, dort ist auch der Ort für Vorschläge, Fehler oder Verbesserungen:

- https://gitlab.com/opentt – gitlab-Gruppe
- https://gitlab.com/groups/opentt/-/issues – Issue-Tracker (Sammler der Gruppe)
