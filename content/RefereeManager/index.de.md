# RefereeManager

![Logo von OpenTT – Schläger mit Katze](../images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

!!! tip ""
	Beste Schiriverwaltung ever.

Der RefereeManager ist ein Programm für die Verwaltung der Schiris des BTTV, die ich (Ekkart) selbst geschrieben habe.
Die vorhandenen Programme reichten mir nicht, insbesondere die fehlende Möglichkeit Massen-E-Mails auch personalisiert zu versenden sowie personalisierte Dokumente zu erzeugen.

Funktionen:

- Verwaltung von Personen mit ihren schirirelevante Daten
- Verwaltung von Terminen, insbesondere Turnieren
- Erzeugung von Dokumenten auf Basis von Freemarker-Templates
- Versenden von E-Mails an einzelne oder mehrere Personen

Was noch fehlt:

- Einsatzplanung
- Statistik über mehrere Saisons
- Handbuch

## Code

- [gitlab-Repository (Code)](https://gitlab.com/opentt/refereemanager)
- [gitlab-Issue-Tracker](https://gitlab.com/opentt/refereemanager/-/issues)
- [Templates](https://gitlab.com/opentt/refereemanager-templates)
