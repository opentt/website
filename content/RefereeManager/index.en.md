# RefereeManager

![Logo of OpenTT – Racket with Cat](../images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

!!! tip ""
	Best referee manager ever.

The RefereeManager is a program for the management of the referees of the BTTV, which I (Ekkart) wrote myself.
The existing programs did not work out for me, especially the missing possibility to send mass emails as well as personalized emails, or create personalized documents.

Functions:

- Management of people with their data relevant to refereeing
- Management of appointments, especially tournaments
- Generation of documents based on Freemarker templates
- Sending of e-mails to single or multiple people

What is missing:

- Assignment planning
- Statistics over several seasons
- Manual

## Code

- [gitlab repository (code)](https://gitlab.com/opentt/refereemanager)
- [gitlab issue tracker](https://gitlab.com/opentt/refereemanager/-/issues)
- [templates](https://gitlab.com/opentt/refereemanager-templates)
