# Links

![Logo von OpenTT – Schläger mit Katze](images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

## OpenTT

- https://gitlab.com/opentt – gitlab-Gruppe
- LARC
	- [Erläuterungen](../larc)
	- https://gitlab.com/opentt/larc-api – Definition von API, Daten und Datenbank
	- https://gitlab.com/opentt/larc – kleiner API-Client
- RefereeManager
	- [Erläuterungen](../RefereeManager)
	- https://gitlab.com/opentt/refereemanager – Desktop-Schiedsrichterverwaltung
	- https://gitlab.com/opentt/refereemanager-templates – Templates für den RefereeManager
- Statistik
	- https://gitlab.com/opentt/statistics – Desktop-Programm
	- https://gitlab.com/opentt/statistics-plugins – Plugins dafür
- [Fragen und Antworten](../70-qna) – Motivation, Ziele, Mitmachen

## Schiedsrichterei

- https://www.tt-schiri.de – Schiedsrichter:innen des BTTV, viele nützliche Downloads (deutsch)
