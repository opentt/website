# Imprint

![Logo of OpenTT – Racket with Cat](images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }
Responsible for the website is Ekkart Kleinod <schiri@ekkart.de>

## Programming, Adaptions

Implementation of the website:

- Ekkart Kleinod
	- gitlab: [ekleinod](https://gitlab.com/ekleinod)
	- E-Mail: <schiri@ekkart.de>

Code and contents:

- <https://gitlab.com/opentt/website>

## Resources

- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- Icons/Graphics
	- [FreeSVG](https://freesvg.org/), license: [Public Domain](https://creativecommons.org/publicdomain/zero/1.0/)
		- Paddle-1294900.svg: https://freesvg.org/paddle-1294900, SVG-ID: 179495
		- gat4.svg: https://freesvg.org/cat-stretching-silhouette-vector-image, SVG-ID: 18280
