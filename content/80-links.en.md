# Links

![Logo of OpenTT – Racket with Cat](images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

## OpenTT

- https://gitlab.com/opentt – gitlab group
- LARC
	- [explanations](../larc)
	- https://gitlab.com/opentt/larc-api – definition of API, data, and database
	- https://gitlab.com/opentt/larc – small API client
- RefereeManager
	- [explanations](../RefereeManager)
	- https://gitlab.com/opentt/refereemanager – referee manager desktop program
	- https://gitlab.com/opentt/refereemanager-templates – templates for the RefereeManager
- Statistics
	- https://gitlab.com/opentt/statistics – desktop programm
	- https://gitlab.com/opentt/statistics-plugins – plugins for the program
- [Questions and Answers](../70-qna) – motivation, goals, participation

## Referee related

- https://www.tt-schiri.de – referees of the BeTTV, many useful downloads (German)
