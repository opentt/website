# Impressum

![Logo von OpenTT – Schläger mit Katze](images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }
Verantwortlicher für diese Webseite: Ekkart Kleinod <schiri@ekkart.de>

## Programmierung, Anpassungen

Umsetzung der Webseite:

- Ekkart Kleinod
	- gitlab: [ekleinod](https://gitlab.com/ekleinod)
	- E-Mail: <schiri@ekkart.de>

Code und Inhalte:

- <https://gitlab.com/opentt/website>

## Ressourcen

- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- Icons/Grafiken
	- [FreeSVG](https://freesvg.org/), Lizenz: [Public Domain](https://creativecommons.org/publicdomain/zero/1.0/)
		- Paddle-1294900.svg: https://freesvg.org/paddle-1294900, SVG-ID: 179495
		- gat4.svg: https://freesvg.org/cat-stretching-silhouette-vector-image, SVG-ID: 18280
