#!/bin/bash
source variables.sh

echo "Build website."
echo

docker run \
	--rm \
	--name $CONTAINERNAME \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--volume "${PWD}:/docs" \
	$IMAGENAME build --strict --verbose
